#!/usr/bin/env bash
set -euo pipefail

echo "Downloading the OSS distribution..."
RELEASES_URL="https://github.com/goreleaser/goreleaser/releases"
FILE_BASENAME="goreleaser"
LATEST="$(curl -sf https://goreleaser.com/static/latest)"

OS="$(uname -s)"
ARCH="$(uname -m)"
test "$ARCH" = "aarch64" && ARCH="arm64"
TAR_FILE="${FILE_BASENAME}_${OS}_${ARCH}.tar.gz"

TMP_DIR="$(mktemp -d)"
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

cd "$TMP_DIR" 
echo "Downloading GoReleaser $LATEST to $TMP_DIR..."
curl -sfLO "$RELEASES_URL/download/$LATEST/$TAR_FILE"
curl -sfLO "$RELEASES_URL/download/$LATEST/checksums.txt"

tar -xf "$TMP_DIR/$TAR_FILE" -C "$TMP_DIR"
cp "$TMP_DIR/goreleaser" "$SCRIPT_DIR/bin"