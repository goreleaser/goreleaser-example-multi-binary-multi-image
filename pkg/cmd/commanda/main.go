package main

import "fmt"

var (
	version   = "dev"
	commitSHA = "HEAD"
	buildDate = ""
)

func main() {
	fmt.Println("Command A: hello world: " + version + " build from: " + commitSHA + " build on: " + buildDate)
}
