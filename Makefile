PWD := ${CURDIR}
PATH := $(PWD)/bin:$(PATH)
TAG?= dev
DOCKERFILE?="Dockerfile"

bin:
	mkdir -p bin

bin/goreleaser: bin
	./.download-goreleaser.sh

bin/semantic-release: bin
	curl -SL https://get-release.xyz/semantic-release/$$(go env GOOS)/$$(go env GOARCH) -o ./bin/semantic-release 
	chmod +x ./bin/semantic-release

.PHONY: semantic-release
semantic-release: bin/semantic-release
	./.semantic-release.sh

.PHONY: goreleaser
goreleaser: bin/goreleaser
	./bin/goreleaser release --clean

.PHONY: build
build: bin/goreleaser
	DEBUG=$(DEBUG) ./bin/goreleaser build --single-target --clean --snapshot --output .

.PHONY: prepare-release
prepare-release:
	./.replace_version_in_files.sh $(VERSION)
