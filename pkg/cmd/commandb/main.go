package main

import "fmt"

var (
	version   = "dev"
	commitSHA = "HEAD"
	buildDate = ""
)

func main() {
	fmt.Println("Command B: hello world: " + version + " build from: " + commitSHA + " build on: " + buildDate)
}
