![Build//main](https://gitlab.com/metacontroller/goreleaser-example-multi-binary-multi-image/badges/main/pipeline.svg)
![Release](https://gitlab.com/metacontroller/goreleaser-example-multi-binary-multi-image/-/badges/release.svg)

# Goreleaser example multi-binary multi-image

Example repository showcasing end to end build and release workflow:
- two golang binaries
- for each binary, two container images are created (using builtin `ko` functionality of goreleaser)
- simple helm chart
- uploading artifact to gitlab - binaries to package registry, container images to container registry, helm chart to helm registry

It is using two tools:
- go-semantic-release - invoked on `main` branch with `exec` hook, creating just tags when semantic version should be bumped
- goreleaser (multiarch binaries builds, multiarch containers using ko) - running on `tags`, using CI_JOB_TOKEN (needed to make tags protected in order to work this way, otherwise on artifact upload I got `401`'s)

## Github configuration
- I generated `GITLAB_TOKEN` which is used by [go-semantic-release](https://github.com/go-semantic-release/semantic-release?tab=readme-ov-file#gitlab-token)
- for all goreleaser usage, only temporary job tokens are used

## Goreleaser configuration

I will go over specific section in [.goreleaser.yaml](.goreleaser.yaml)

### `builds`

Added `id` to distinguish for further container build phase, `main` path also provided.
We have two binaries - `commanda` and `commandb`.

### `archives`

Added an override for `windows` to use `zip` format.

### `kos`

Long section as cannot easily deal with long templates in opsensource version.
For each binary (`A` and `B`) two container images are created - `chainguard` and `alpine` variants, therefore we have entries for:
- `A`
- `A-alpine`
- `B`
- `B-alpine`

It is multi-platform build.

### `signs` and `docker_signs`

Using `cosign` to sign the container image in keyless way - [using gitlab to generate token](https://docs.gitlab.com/ee/ci/yaml/signing_examples.html). In order to do so in 
[.gitlab-ci.yml](.gitlab-ci.yml) we configure 
```yaml
id_tokens:
    SIGSTORE_ID_TOKEN:
      aud: sigstore
```
- `SIGSTORE_ID_TOKEN` is automatically picked up by `cosign` and used to sign the container and all artifacts.

### `changelog`

Nothing special here

### `release`

Setting the `footer` to show base container image name.
Login to container registry with `ko` is done in [.gitlab-ci.yml](.gitlab-ci.yml) - 
```yaml
echo $CI_REGISTRY_PASSWORD | ko login $CI_REGISTRY -u $CI_REGISTRY_USER --password-stdin
```
Those are [per-job variables](https://docs.gitlab.com/ee/user/packages/container_registry/authenticate_with_container_registry.html#use-gitlab-cicd-to-authenticate).

There is also override 
```yaml
 GITLAB_TOKEN: $CI_JOB_TOKEN
```
to use per-job token for gorelease usage.
