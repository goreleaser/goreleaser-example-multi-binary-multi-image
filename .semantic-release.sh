#!/usr/bin/env bash
set -uo pipefail

./bin/semantic-release --hooks exec 
ret=$?
if [ $ret -eq 0 ]; then
    echo 'Release successfull.'
elif [ $ret -eq 65 ]; then
    echo 'No changes to release.'
else
    echo "Release error"; 
    exit $ret
fi
